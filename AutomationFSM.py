from .controllers import TaskControl
from .CrabAPI import CrabAPI
import enum
import os
import os.path
import logging
import sys

class AutomationFSM:
    def __init__(self, workflow, campaign, proxy, TaskStatusClass):
        self.workflow = workflow
        self.campaign = campaign
        self.TaskStatusClass = TaskStatusClass
        self.proxy = proxy
        self.task_controller = TaskControl(campaign=campaign, workflow=workflow, TaskStatusClass=TaskStatusClass)

        self.crabAPI = CrabAPI(campaign, workflow, proxy)

        logging.basicConfig(
            level=logging.INFO,
            format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
            handlers=[
                logging.FileHandler("debug.log"),
                logging.StreamHandler(sys.stdout)
            ]
        )

        logging.info(f"Starting WORKFLOW={workflow}, CAMPAIGN={campaign}")

    def process_new_tasks(self, task_list):
        tasks_list = set(task_list)
        tasks_in_database = self.task_controller.getAllTasks().get_points()
        tasks_in_database = set(map(lambda x: int(x['dataPeriod']), tasks_in_database))
        tasks_not_submited_yet = tasks_list-tasks_in_database
        logging.info(f"New tasks {tasks_not_submited_yet}")
        if tasks_not_submited_yet:
            self.task_controller.submitTasks(tasks_not_submited_yet)

    def perform_action(self, state, data_period, task_information):
        raise NotImplementedError()

    def __filter_state(self, entry):
        key = entry[0]
        value = entry[1]

        return key in self.TaskStatusClass.__members__.keys() and value == 1

    def process_tasks(self):
        tasks_in_database_information_list = self.task_controller.getAllTasks().get_points()
        for task_information in tasks_in_database_information_list:
            _task_information = task_information
            while True:
                _state = list(filter(self.__filter_state, _task_information.items()))[0][0]
                data_period = _task_information['dataPeriod']

                #drop status and workflow infromation
                keys_to_remove = list(self.TaskStatusClass.__members__.keys())
                keys_to_remove.extend(['dataPeriod', 'campaign', 'workflow', 'time'])
                for state in keys_to_remove:
                    if state in _task_information:
                        del _task_information[state]

                keys = list(_task_information.keys())
                for key in keys:
                    if key not in vars(self.TaskStatusClass) or key.startswith("_"):
                        del _task_information[key]

                logging.info(f"Processing DATA_PERIOD={data_period}, STATE={_state}, DATA={_task_information}")
                new_state, new_task_information = self.perform_action(_state, data_period, _task_information)

                #update data
                if new_state != _state or new_task_information != _task_information:
                    logging.info(f"New state and data STATE={new_state}, DATA={new_task_information}")
                    _task_information = self.task_controller.update(data_period, _task_information, new_state)
                else:
                    logging.info(f"State and data were not changed")

                if new_state == _state:
                    logging.info(f"Stop processing DATA_PERIOD={data_period}")
                    break

    def run(self, task_list):
        self.process_new_tasks(task_list)
        self.process_tasks()

        for task_information in self.task_controller.getAllTasks().get_points():
            if task_information['stop'] != 1:
                return False
        return True