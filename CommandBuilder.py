import subprocess

class Command:
    def __init__(self, command):
        self.command = f"( {command}"
        self.next = None
        self.pipe = None

    def args(self, *args, **kvargs):
        for arg in args:
            self.command += f" {arg}"
        for arg,val in kvargs.items():
            self.command += f" {arg}={val}"
        return self

    def then(self, command):
        self.pipe = None
        self.next = command
        return self

    def __or__(self, command):
        self.next = None
        self.pipe = command
        return self

    def build(self):
        return self.command + ((f" | {self.pipe.build()} )" if self.pipe else f" && {self.next.build()} )") if (self.pipe or self.next) else " )")

    def run(self, out=False):
        command = self.build()
        process = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE)
        process.wait()
        return (process.returncode, str(process.communicate()[0], "utf-8")) if out else process.returncode