from .controllers import JobCtrl
from .CrabInterface import CrabInterface
import subprocess
import os

class CrabAPI:
    def __init__(self, campaign, workflow, proxy):
        self.campaign = campaign
        self.workflow = workflow
        self.jobCtrl = JobCtrl(workflow, campaign)
        self.crabInterface = CrabInterface(campaign, workflow, proxy)

    def submit(self, psetName, data_period, name, runs_range, template_path, dataset, lfn="", inputFiles="", args=""):
        return self.crabInterface.submit(psetName, data_period, name, runs_range, template_path, dataset, lfn, inputFiles, args) == 0

    def is_finished(self, data_period, successRatio=1.0):
        result = self.crabInterface.is_finished(data_period, successRatio)
        if result == 0:
            self.jobCtrl.deleteJobsFromDatabase(data_period)

        return result == 0

    def status(self, data_period, name):
        return self.crabInterface.status(data_period, name)