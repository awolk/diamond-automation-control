#!/usr/bin/env python
"""
This is a small script that does the equivalent of multicrab.
"""

import os

import sys
from optparse import OptionParser
from io import BytesIO as StringIO
import time

import CRABClient
from CRABAPI.RawCommand import crabCommand
from CRABClient.ClientExceptions import ClientException


#MONKEY PATCHING!
from CRABClient.Commands.submit import submit
import shutil
import tempfile
from ServerUtilities import downloadFromS3, getDownloadUrlFromS3, downloadFromS3ViaPSU
import os
import sys
import json


import re
import shlex
import shutil
import tarfile
import tempfile
if sys.version_info >= (3, 0):
    from urllib.parse import urlencode, quote  # pylint: disable=E0611
if sys.version_info < (3, 0):
    from urllib import urlencode, quote

def monkey_patch(self, filecacheurl, uniquerequestname):
    """
    Downloads the dry run tarball from the User File Cache and unpacks it in a temporary directory.
    Runs a trial to obtain the performance report. Repeats trial with successively larger input events
    until a job length of maxSeconds is reached (this improves accuracy for fast-running CMSSW parameter sets.)
    """
    cwd = os.getcwd()
    try:
        tmpDir = tempfile.mkdtemp()
        self.logger.info('Created temporary directory for dry run sandbox in %s' % tmpDir)
        os.chdir(tmpDir)
        if 'S3' in filecacheurl.upper():
            print(self.crabserver, os.path.join(tmpDir, 'dry-run-sandbox.tar.gz'), uniquerequestname)
            downloadFromS3(crabserver=self.crabserver, filepath=os.path.join(tmpDir, 'dry-run-sandbox.tar.gz'),
                           objecttype='runtimefiles', taskname=uniquerequestname, logger=self.logger)
        else:
            ufc = CRABClient.Emulator.getEmulator('ufc')({'endpoint' : filecacheurl, "pycurl": True})
            ufc.downloadLog('dry-run-sandbox.tar.gz', output=os.path.join(tmpDir, 'dry-run-sandbox.tar.gz'))
        for name in ['dry-run-sandbox.tar.gz', 'InputFiles.tar.gz', 'CMSRunAnalysis.tar.gz', 'sandbox.tar.gz']:
            tf = tarfile.open(os.path.join(tmpDir, name))
            tf.extractall(tmpDir)
            tf.close()
        env = os.environ.update({'CRAB3_RUNTIME_DEBUG': 'True', '_CONDOR_JOB_AD': 'Job.submit'})

        with open('splitting-summary.json') as f:
            splitting = json.load(f)

        if self.options.skipEstimates:
            return splitting, None

        self.logger.info('Executing test, please wait...')

        events = 10
        totalJobSeconds = 0
        maxSeconds = 25
        while totalJobSeconds < maxSeconds:
            optsList = getCMSRunAnalysisOpts('Job.submit', 'RunJobs.dag', job=1, events=events)
            # from a python list to a string which can be used as shell command argument
            opts = ''
            for opt in optsList:
                opts = opts + ' %s' % opt
            command = env + 'sh CMSRunAnalysis.sh ' + opts
            out, err, returncode = execute_command(command=command)
            self.logger.debug(out)
            if returncode != 0:
                raise ClientException('Dry run failed to execute local test run:\n StdOut: %s\n StdErr: %s' % (out, err))

            #Once this https://github.com/dmwm/CRABServer/pull/4938 will get merged the job will be executed inside the CMSSW dir
            #Therefore the 'jobReport.json' will not be in the cwd. We will delete these three lines of code in the future
            jobReport = 'jobReport.json'
            if not os.path.isfile(jobReport):
                jobReport = os.path.join(self.configreq["jobsw"], jobReport)
            with open(jobReport) as f:
                report = json.load(f)['steps']['cmsRun']['performance']
            events += (maxSeconds / float(report['cpu']['AvgEventTime']))
            totalJobSeconds = float(report['cpu']['TotalJobTime'])

    finally:
        os.chdir(cwd)
        shutil.rmtree(tmpDir)

    return splitting, report
        
        
def monkey_patch2(crabserver=None, filepath=None, objecttype=None, taskname=None,
                    username=None, tarballname=None, logger=None):
    """
    one call to make a 2-step operation:
    obtains a preSignedUrl from crabserver RESTCache and use it to download a file
    :param crabserver: a RESTInteraction/CRABRest object : points to CRAB Server to use
    :param filepath: string : the full path of the file to create with the downloaded content
        if file exists already, it is silently overwritten
    :param objecttype: string : the kind of object to dowbload: clientlog|twlog|sandbox|debugfiles|runtimefiles
    :param taskname: string : the task this object belongs to, if applicable
    :param username: string : the username this sandbox belongs to, in case objecttype=sandbox
    :param tarballname: string : for sandbox, taskname is not used but tarballname is needed
    :return: nothing. Raises an exception in case of error
    """
    preSignedUrl = getDownloadUrlFromS3 (crabserver=crabserver, objecttype=objecttype,
            taskname=taskname, username=username, tarballname=tarballname, logger=logger)
    time.sleep(100)
    downloadFromS3ViaPSU(filepath=filepath, preSignedUrl=preSignedUrl, logger=logger)
    return      
        
        
submit.executeTestRun = monkey_patch
downloadFromS3 = monkey_patch2



from httplib import HTTPException

import traceback
import subprocess

def getOptions():
    """
    Parse and return the arguments provided by the user.
    """
    usage = ("Usage: %prog --crabCmd CMD [--workArea WAD --crabCmdOpts OPTS]"
             "\nThe multicrab command executes 'crab CMD OPTS' for each project directory contained in WAD"
             "\nUse multicrab -h for help")

    parser = OptionParser(usage=usage)

    parser.add_option('-m', '--crabCmd',
                      dest = 'crabCmd',
                      default = '',
                      help = "crab command",
                      metavar = 'CMD')

    parser.add_option('--workArea',
                      dest = 'workArea',
                      default = '',
                      help = "work area directory (only if CMD != 'submit')",
                      metavar = 'WAD')

    parser.add_option('-o', '--crabCmdOpts',
                      dest = 'crabCmdOpts',
                      default = '',
                      help = "options for crab command CMD",
                      metavar = 'OPTS')

    parser.add_option('-t', '--template',
                      dest = 'template',
                      default = '',
                      help = "crab config template",
                      metavar = 'TEMPLATE')

    parser.add_option('-c', '--campaign',
                      dest = 'campaign',
                      default = '',
                      help = "Automation campaign",
                      metavar = 'CAMPAIGN')

    parser.add_option('-d', '--inputDatasets',
                      dest = 'inputDatasets',
                      default = '',
                      help = "Data to be processed",
                      metavar = 'DATASETS')
                      
    parser.add_option('-r', '--runsRange',
                      dest = 'runsRange',
                      default = '',
                      help = "range of runs",
                      metavar = 'RUNSRANGE')

    parser.add_option('-l', '--lumiMask',
                      dest = 'lumiMask',
                      default = '',
                      help = "Data to be processed",
                      metavar = 'LUMIMASK')
                      
    parser.add_option('--dataPeriod',
                      dest = 'dataPeriod',
                      default = '',
                      help = "Number of data period being processed",
                      metavar = 'DATAPERIOD')

    parser.add_option('--proxy',
                      dest = 'proxy',
                      default = '',
                      help = 'proxy file',
                      metavar = 'PROXY')

    parser.add_option('--lfn',
                      dest = 'lfn',
                      default = '',
                      help = 'output folder for CRAB',
                      metavar = 'LFN')
    
    parser.add_option('-w', '--workflow',
                      dest = 'workflow',
                      default = '',
                      help = "workflow name",
                      metavar = 'WORKFLOW')

    parser.add_option('-a', '--args',
                      dest = 'args',
                      default = '',
                      help = 'additional arguments for cmssw separated by space',
                      metavar = 'ARGS')

    parser.add_option('-i', '--input',
                      dest = 'input',
                      default = '',
                      help = 'input files for cmssw separated by collon',
                      metavar = 'INPUT')

    parser.add_option('-n', '--name',
                      dest = 'name',
                      default = '',
                      help = "Unique request name",
                      metavar = 'NAME')

    parser.add_option('-p', '--pset',
                      dest = 'pset',
                      default = '',
                      help = "pset path",
                      metavar = 'PSET')
    
                      
    (options, arguments) = parser.parse_args()

    options.inputDatasets = options.inputDatasets.split(',')

    if arguments:
        parser.error("Found positional argument(s): %s." % (arguments))
    if not options.crabCmd:
        parser.error("(-c CMD, --crabCmd=CMD) option not provided.")
    if options.crabCmd != 'submit':
        if not options.workArea:
            parser.error("(-w WAR, --workArea=WAR) option not provided.")
        if not os.path.isdir(options.workArea):
            parser.error("'%s' is not a valid directory." % (options.workArea))

    return options


def main():

    options = getOptions()

    # The submit command needs special treatment.
    if options.crabCmd == 'submit':

        #--------------------------------------------------------
        # This is the base config:
        #--------------------------------------------------------
        import imp
        template = imp.load_source('template', options.template)

        config = template.config
        config.Data.lumiMask             = options.lumiMask
        #--------------------------------------------------------

        for inDS in options.inputDatasets:
            args = options.args.split(":") if options.args != "" else []
            files = options.input.split(":") if options.input != "" else []
            # print(args)
            # print(files)
            # print(options.lfn)
            #x = 2 / 0

            # NOT USED ANYMORE!!! inDS is of the form /A/B/C. Since B is unique for each inDS, use this in the CRAB request name.
            #config.General.requestName = 'PHISYM-'+options.campaign+'-'+inDS.split('/')[2]+'_fill_'+str(options.dataPeriod)
            config.General.requestName = '_'.join([options.campaign, options.workflow, options.dataPeriod, options.name])
            config.JobType.psetName = options.pset
            config.JobType.scriptExe  = os.path.dirname(os.path.realpath(__file__))+'/wrapper.sh'
            config.JobType.scriptArgs = ['-c='+options.campaign, '-w='+options.workflow, '--dataPeriod='+options.dataPeriod]
            config.JobType.pyCfgParams = args
            config.JobType.inputFiles = files
            config.Data.inputDataset = inDS
            config.Data.runRange = options.runsRange
            if options.lfn != '':
                config.Data.outLFNDirBase = options.lfn

            # Submit.
            crabstdout = StringIO()
            
            try:
                #print("Getting number of jobs for dataset %s" % (inDS))
                # ugly parsing of crab output to get the number of jobs

                sys_stdout = sys.stdout                
                sys.stdout = crabstdout

                dry_sub_opt = options.crabCmdOpts.split()+['--dryrun', '--skip-estimates']
                ret = crabCommand(options.crabCmd, config = config, proxy=options.proxy, *dry_sub_opt)
                
                print(crabstdout.getvalue())
                n_jobs = list(filter(lambda line : line if 'Task consists of' in line else None, crabstdout.getvalue().split('\n')))[0].split()[3]
                
                # register task into ecal automation db (note crab id numbering starts from 1!)
                
                #subprocess.check_call(['python3', os.path.dirname(os.path.realpath(__file__))+'/AutomationScript.py', '-c', str(options.campaign), '-w', str(options.workflow), '--dataPeriod', options.dataPeriod, 'submit', '--ids', ','.join([str(i+1) for i in range(int(n_jobs))])], stderr=subprocess.STDOUT)
                subprocess.check_call(['python3', '-m', 'FSM.AutomationScript', '-c', str(options.campaign), '-w', str(options.workflow), '--dataPeriod', options.dataPeriod, 'submit', '--ids', ','.join([str(i+1) for i in range(int(n_jobs))])], stderr=subprocess.STDOUT)

                # actual submit
                crabCommand('proceed', dir=os.path.abspath('crab_'+config.General.requestName), proxy=options.proxy, *options.crabCmdOpts.split())
            except HTTPException as hte:
                print("Submission for input dataset %s failed: %s" % (inDS, hte.headers))
                sys.exit(1)
            except ClientException as cle:
                print(cle)
                print("Submission for input dataset %s failed: %s" % (inDS, cle))
                sys.exit(1)
            except subprocess.CalledProcessError as err:
                print("Submission for input dataset %s failed: %s" % (inDS, err.returncode))
                sys.exit(1)
            except Exception as e:
                print("ERROR")
                print(e)
                traceback.print_exc()
                sys.exit(1)
    # All other commands can be simply executed.
    elif options.workArea:

        for dir in os.listdir(options.workArea):
            projDir = os.path.join(options.workArea, dir)
            if not os.path.isdir(projDir):
                continue
            # Execute the crab command.
            msg = "Executing (the equivalent of): crab %s --dir %s %s" % (options.crabCmd, projDir, options.crabCmdOpts)
            print("-"*len(msg))
            print(msg)
            print("-"*len(msg))
            try:
                crabCommand(options.crabCmd, dir = projDir, *options.crabCmdOpts.split())
            except HTTPException as hte:
                print("Failed executing command %s for task %s: %s" % (options.crabCmd, projDir, hte.headers))
                sys.exit(1)
            except ClientException as cle:
                print("Failed executing command %s for task %s: %s" % (options.crabCmd, projDir, cle))
                sys.exit(1)


if __name__ == '__main__':
    main()
    sys.exit(0)