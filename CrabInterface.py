import logging
import subprocess
import os
import re
from .CommandBuilder import Command

class CrabInterface:
    def __init__(self, campaign, workflow, proxy):
        self.campaign = campaign
        self.workflow = workflow
        self.proxy = proxy

    def submit(self, psetName, data_period, name, runs_range, template_path, dataset, lfn="", inputFiles="", args=""):
        # result = subprocess.run(['python', os.path.dirname(os.path.realpath(__file__))+'/CrabCommandsWrapperScript.py', '--crabCmd', 'submit', '-d', dataset, 
        # '-r', runs_range,'-t', template_path, '-c', self.campaign, '--input', inputFiles, '--args', args,
        # '-w', self.workflow, '--dataPeriod', data_period, '--lfn', lfn, '--name', name], stderr=subprocess.STDOUT, env=os.environ)
        python = Command("python").args(os.path.dirname(os.path.realpath(__file__))+'/CrabCommandsWrapperScript.py',
                                   '--crabCmd submit',
                                   f"-p {psetName}",
                                   f"-d {dataset}",
                                   f"-r {runs_range}",
                                   f"-t {template_path}",
                                   f"-c {self.campaign}",
                                   f"-w {self.workflow}",
                                   f"--dataPeriod {data_period}",
                                   f"--name {name}",
                                   f"--proxy {self.proxy}")
        
        if inputFiles != "":
            python.args(f"--input {inputFiles}")

        if args != "":
            python.args(f"--args {args}")

        if lfn != "":
            python.args(f"--lfn {lfn}")

        logging.info(f"Submit CRAB job: {python.build()}")

        return python.run()

    def is_finished(self, data_period, successRatio=1.0):
        # result = subprocess.run(['python3', '-m', 'FSM.AutomationScript', '-c', self.campaign, 
        # '-w', self.workflow, '--dataPeriod', data_period, 'wait', '--successRatio', str(successRatio), 
        # "--resubcmd", "crab resubmit --proxy=/afs/cern.ch/user/e/ecalgit/grid_proxy.x509 crab_wdir", 
        # "--howManyAttempts", "1", "-s", "0" ], stderr=subprocess.STDOUT)
        python = Command("python3").args("-m FSM.AutomationScript",
                                    f"-c {self.campaign}",
                                    f"-w {self.workflow}",
                                    f"--dataPeriod {data_period}",
                                    "wait",
                                    f"--successRatio {str(successRatio)}",
                                    "--resubcmd \"crab resubmit --proxy=/afs/cern.ch/user/e/ecalgit/grid_proxy.x509 crab_wdir\"",
                                    "--howManyAttempts 1",
                                    "-s 0")
        return python.run()

    def status(self, data_period, name):
        # process = subprocess.Popen(["crab", "status", f"crab_{self.campaign}_{self.workflow}_{str(data_period)}_{name}", "--long"], stdout=subprocess.PIPE)
        # stdout = process.communicate()[0]
        # stdout = str(stdout, "utf-8")
        crab = Command("crab").args("status", f"-d ./crab_{self.campaign}_{self.workflow}_{str(data_period)}_{name}", "--long", f"--proxy {self.proxy}")
        ret, stdout = crab.run(out=True)
        finished = list(map(lambda e: int(e.split(' ')[0]), re.findall(f'\d+ finished', stdout)))
        failed = list(map(lambda e: int(e.split(' ')[0]), re.findall(f'\d+ failed', stdout)))

        logging.info(f"Transfer status for ./crab_{self.campaign}_{self.workflow}_{str(data_period)}_{name}: finished={finished}, failed={failed}")

        return finished, failed