import sys
import enum
from copy import deepcopy as dcopy
from datetime import datetime

from influxdb import InfluxDBClient
import urllib3
urllib3.disable_warnings()

from .credentials import *

class JobStatus(enum.Enum):
    """
    Simple class to encode job statuses
    """

    idle = enum.auto(),
    running = enum.auto(),
    failed= enum.auto(),
    done = enum.auto()

class JobCtrl:
    """
    Update job status and information to the influxdb. 
    Each combination of campaign+workflow is called a task.

    The job status is represented by the following table::

       {
           'measurement' : 'job',
           'tags' : {
               'workflow' : 'ECAL offline workflow name',
               'campaign' : 'Campaign to which the job belongs to',
               'id'       : 'Progressive number starting from 0 that identify the current job within the task'
               'dataPeriod'   : 'data period number'
           },
           'time' : timestamp,
           'fields' : {
               'idle' : boolean,
               'running' : boolean,
               'failed' : boolean,
               'done' : boolean
           }        
       }

    :param workflow: name of the workflow (e.g. ECALELF_prod, ECALELF_ntuples, PHISYM_prod, PHISYM_merge, ...)
    :type workflow: string.
    :param campaign: name of the processing campaign.
    :type campaign: string.
    """

    def __init__(self, workflow=None, campaign=None):
        """                                                                                                                                                         Create a new JobCtrl workflow                                                                                                                       
        """

        ### allow only pre-determined workflows
        if workflow==None:
            sys.exit('[JobCtrl::init] The workflow field is mandatory')

        ### require a campaign
        if campaign==None:
            sys.exit('[JobCtrl::init] The campaign field is mandatory')

        ### create point data template
        self.global_data = {
            'measurement' : None,
            'tags' : {
                'workflow' : str(workflow),
                'campaign' : str(campaign),
                'dataPeriod': 0
            },
            'time' : None,
            'fields' : {
                'idle' : 0,
                'running' : 0,
                'failed' : 0,
                'done' : 0
            }
        }            
        self.db = InfluxDBClient(host=dbhost, port=dbport, username=dbusr, password=dbpwd, ssl=dbssl, database=dbname)

        
    def createJobsForTask(self, ids=[], data_period=None):
        """
        Set newly submitted jobs to idle state. This method should be called by the submit script.

        :param ids: submitted job IDs, defaults to [].
        :type ids: list
        :rtype: bool
        """

        ### check ids
        if len(ids) < 1:
            sys.exit('[JobCtrl::createTask] Job ids list is empty')

        ### insert jobs with status set to idle
        data = [dcopy(self.global_data) for _ in range(len(ids))]
        subtime = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        for i,id in enumerate(ids):
            data[i]['measurement'] = 'job'
            data[i]['time'] = subtime
            data[i]['tags']['id'] = str(id)
            data[i]['tags']['dataPeriod'] = data_period
            data[i]['fields'][JobStatus.idle.name] = 1
            
        # data.append(task_data)
        return self.db.write_points(data)

    def getJob(self, id, data_period=None):
        """
        Retrive from the influx db the information of a single job in the current task.

        :param id: the job id.
        :type id: int.
        :return: a dictionary containing the job information.
        :rtype: dict
        """

        jobs_query = self.db.query('SELECT * FROM "job" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' AND "id"=\'%s\' AND "dataPeriod"=\'%s\' ' % 
                                   (self.global_data['tags']['workflow'], self.global_data['tags']['campaign'], 
                                   str(id), data_period))

        return jobs_query

    def getJobs(self, data_period=None):
        """
        Retrive from the influx db all jobs belonging to the current task

        :return: a dictionary containing the list of job ids in a given status.
        :rtype: dict
        """
        
        jobs_query = self.db.query('SELECT last("running") AS "running", last("idle") AS "idle", last("failed") AS "failed", last("done") AS "done" FROM "job" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' AND "dataPeriod"=\'%s\' GROUP BY "id"' % 
                             (self.global_data['tags']['workflow'], self.global_data['tags']['campaign'], data_period))

        jobs = {
            "idle" : [],
            "running" : [],
            "failed" : [],
            "done" : [],
        }
        for id,job in jobs_query.items():
            data = next(job)
            for status, v in jobs.items():
                if data[status] > 0:
                    v.append(id[1]['id'])

        return jobs

    def setStatus(self, id=None, data_period=None, status=JobStatus.idle):
        """
        Set status of job #id.
        This methods (or its shortcuts) should be called by the job itself.
        
        :param id: job id within the submission, defaults to None.
        :type id: int
        :param status: job new status, defaults to JobStatus.idle.
        :type status: JobStatus
        :rtype: bool
        """

        ### check id
        if id==None:
            sys.exit('[JobCtrl::setStatus] Please specify a vaild job id')

        ### check if job already exist in db (should have been injected by createTask)
        exist = len(self.db.query('SELECT * FROM "%s" WHERE "workflow" = \'%s\' AND "campaign" = \'%s\' AND "id" = \'%s\' AND "dataPeriod"=\'%s\' ' % 
                                  ('job', self.global_data['tags']['workflow'], 
                                   self.global_data['tags']['campaign'], str(id), data_period)))
        if not exist:
            sys.exit('[JobCtrl::setStatus] Job %s not found in %s+%s task. Please submit the task first using JobCtrl::createTask' % 
                     (str(id), self.global_data['tags']['workflow'], self.global_data['tags']['campaign']))

        ### check status
        if status.name not in JobStatus.__members__.keys():
            sys.exit('[JobCtrl::setStatus] Specified status %s is not valid. Valid statuses are: \n\t%s' % (status, '\n\t'.join(JobStatus.__members__.keys())))

        data = dcopy(self.global_data)
        data['measurement'] = 'job'
        data['time'] = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        data['tags']['id'] = str(id)
        data['tags']['dataPeriod'] = str(data_period)
        data['fields'][status.name] = 1

        ### check data consistency
        if sum([data['fields'][sts] for sts in JobStatus.__members__.keys()]) != 1:
            sys.exit('[JobCtrl::setStatus] More then one status is being set for the same job (id=%s)' % str(id))         

        return self.db.write_points([data])

    def getIdle(self, data_period=None):
        """
        Get all jobs in the current task in idle state.

        :return: the list of job ids in idle state.
        :rtype: list
        """

        return self.getJobs(data_period)['idle']

    def getRunning(self, data_period=None):
        """
        Get all jobs in the current task in running state.

        :return: the list of job ids in running state.
        :rtype: list
        """

        return self.getJobs(data_period)['running']

    def getFailed(self, data_period=None):
        """
        Get all jobs in the current task in failed state.

        :return: the list of job ids in failed state.
        :rtype: list
        """

        return self.getJobs()['failed']

    def getDone(self, data_period=None):
        """
        Get all jobs in the current task in done state.

        :return: the list of job ids in done state.
        :rtype: list
        """

        return self.getJobs(data_period)['done']

    def idle(self, id=None, data_period=None):
        """
        Set job #id status to idle

        :param id: job id within the submission, defaults to None.
        :type id: int
        """

        self.setStatus(id=id, status=JobStatus.idle)

    def running(self, id=None, data_period=None):
        """
        Set job #id status to running

        :param id: job id within the submission, defaults to None.
        :type id: int
        """

        self.setStatus(id=id, data_period=data_period, status=JobStatus.running)

    def failed(self, id=None, data_period=None):
        """
        Set job #id status to failed

        :param id: job id within the submission, defaults to None.
        :type id: int
        """

        self.setStatus(id=id, data_period=data_period, status=JobStatus.failed)

    def done(self, id=None, data_period=None):
        """
        Set job #id status to done

        :param id: job id within the submission, defaults to None.
        :type id: int
        """

        self.setStatus(id=id, data_period=data_period, status=JobStatus.done)
                
    def deleteJobsFromDatabase(self, data_period=None):
        self.db.query('DELETE FROM "job" WHERE "workflow"=\'%s\' AND "campaign"=\'%s\' AND "dataPeriod"=\'%s\' '% 
                     (self.global_data['tags']['workflow'], self.global_data['tags']['campaign'], data_period))
